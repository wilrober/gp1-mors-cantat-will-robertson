﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Player Character", menuName = "Player Character")]
public class Player : ScriptableObject
{
				public new string name;

				public Sprite playerArt;

				public Skill[] skills = new Skill[4];

				public int health;

				public void Print()
				{
								Debug.Log(name + ": " + health + " hp : ");
				}
}
