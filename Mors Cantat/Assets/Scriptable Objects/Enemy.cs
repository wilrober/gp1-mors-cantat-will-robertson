﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "Enemy")]
public class Enemy : ScriptableObject
{
				public new string name;

				public Sprite enemyArt;

				public Skill attack;

				public int attackDelay;
				public int health;
				public int difficulty;

				public EnemyController enemyScript;

				public void Print()
				{
							Debug.Log(name + ": " + health + " hp : " + difficulty + " power");
				}
}
