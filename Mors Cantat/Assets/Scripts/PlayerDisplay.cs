﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerDisplay : MonoBehaviour
{
				public Player player;
				PlayerController playerScript;

				public TextMeshProUGUI nameText;
				public TextMeshProUGUI healthText;

				public GameManager gameManager;

				public Sprite playerSprite;
				
				void Start()
    {
								gameManager = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameManager>();

								player = gameManager.chosenPlayer;

								playerScript = GetComponent<PlayerController>();

								playerSprite = player.playerArt;
								healthText.text = "";
								nameText.text = "";
				}

				private void Update()
				{
								healthText.text = ("HP: " + playerScript.currentHealth.ToString());
								nameText.text = player.name;
				}
}
