﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour
{
   public void StartGame()
				{
								SceneManager.LoadScene(1);
				}

				public void Quit()
				{
								Application.Quit();
								Debug.Log("Game Closed");
				}
}