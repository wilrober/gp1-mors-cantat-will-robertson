﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EnemyDisplay : MonoBehaviour
{
				public Enemy enemy;
				EnemyController script;

				public TextMeshProUGUI nameText;
				public TextMeshProUGUI healthText;

				public GameManager gameManager;

				public Sprite enemySprite;

    void Start()
    {
								script = GetComponent<EnemyController>();

								gameManager = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameManager>();

								enemy = gameManager.enemyTemplates[script.enemyID];

								enemySprite = enemy.enemyArt;
								healthText.text = "";
								nameText.text = "";
				}

				private void OnMouseOver()
				{
								healthText.text = ("HP: " + script.currentHealth.ToString());
								nameText.text = enemy.name;
				}

				private void OnMouseExit()
				{
								healthText.text = "";
								nameText.text = "";
				}

				public void ResetEnemy()
				{
								enemySprite = enemy.enemyArt;
				}
}
