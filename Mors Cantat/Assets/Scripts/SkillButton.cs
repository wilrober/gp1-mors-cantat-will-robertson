﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SkillButton : MonoBehaviour
{
				public PlayerDisplay playerDisplay;

				public int buttonNumber;

				public TextMeshProUGUI toolTip;

				string infoText;

    // Start is called before the first frame update
    void Start()
    {
								playerDisplay = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerDisplay>();

								toolTip = GameObject.FindGameObjectWithTag("Tool Tip").GetComponent<TextMeshProUGUI>();

								while (infoText == null)
								{
												Debug.Log("Finding information text...");
												infoText = playerDisplay.player.skills[buttonNumber - 1].ToString();
								}
				}

				public void OnMouseOver()
				{
								toolTip.text = infoText;
				}

				public void OnMouseExit()
				{
								toolTip.text = "";
				}
}