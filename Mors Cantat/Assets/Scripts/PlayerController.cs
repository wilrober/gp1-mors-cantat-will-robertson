﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
				public Player playerObject;

				public PlayerDisplay playerDisplay;

    public int health;
				public int currentHealth;
    public Skill[] skillsArray;
    public Skill currentSkill;

				SpriteRenderer spriteRender;

				public bool initialized = false;

    // Start is called before the first frame update
    void Start()
    {
								
				}

    // Update is called once per frame
    void Update()
    {
								if (playerObject == null)
								{
												Debug.Log("Finding player object...");

												playerDisplay = GetComponent<PlayerDisplay>();

												playerObject = playerDisplay.player;

												skillsArray = playerObject.skills;

												health = playerObject.health;
												currentHealth = health;

												currentSkill = skillsArray[0];
												Debug.Log(skillsArray[0].value + "\n" + currentSkill);

												spriteRender = gameObject.GetComponent<SpriteRenderer>();
												spriteRender.sprite = playerObject.playerArt;
								}
								initialized = true;
				}

				public void ChangeActiveSkill(int activeIndex)
				{
								if(activeIndex > skillsArray.Length)
								{
												Debug.LogError("There are a max of 4 skills.");
								}
								currentSkill = skillsArray[activeIndex];
								Debug.Log(currentSkill);
				}

				public void OnMouseDown()
				{
								if (!currentSkill.attack)
								{
												currentHealth += currentSkill.value;
								}
								else
								{
												Debug.Log("This is not a valid target for this skill");
								}
				}
}