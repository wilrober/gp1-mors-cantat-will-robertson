﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public GameObject mainCamera;
				public GameObject mainCanvas;
				public Player chosenPlayer;
				public GameObject[] enemies;

				public PlayerController playerScript;

				public GameObject mainMenuButton;
				public GameObject nextAreaButton;

				public TextMeshProUGUI areaText;

				public int areaNumber = 0;

				public string[] areaLabels;

				public Enemy[] enemyTemplates;

    // Start is called before the first frame update
    void Start()
    {
								areaLabels = new string[] { "The Plains", "The Moor", "The Gate", "The Castle", "The Catacombs", "The Chamber"};

								mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
								mainCanvas = GameObject.FindGameObjectWithTag("Canvas");
								enemies = GameObject.FindGameObjectsWithTag("Enemy");

								playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

								areaText.text = areaLabels[areaNumber];

								nextAreaButton.SetActive(false);
								mainMenuButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
								if (AllEnemiesDead())
								{
												nextAreaButton.SetActive(true);
								}

								if(playerScript.currentHealth <= 0 && playerScript.initialized)
								{
												playerScript.currentHealth = 0;
												areaText.text = "You've died!";
												mainMenuButton.SetActive(true);
								}
    }

				private bool AllEnemiesDead()
				{
								for (int i = 0; i < enemies.Length; i++)
								{
												if (!enemies[i].GetComponent<EnemyController>().dead)
												{
																return false;
												}
								}
								return true;
				}

				public void LoadNextArea()
				{
								areaNumber++;
								if (areaNumber == areaLabels.Length-1)
								{
												areaText.text = areaLabels[areaNumber];
												enemies[0].GetComponent<EnemyController>().enemyObject = enemyTemplates[3];
												enemies[0].GetComponent<EnemyController>().ResetEnemy();
												enemies[0].GetComponent<EnemyDisplay>().enemy = enemyTemplates[3];
												enemies[0].GetComponent<EnemyDisplay>().ResetEnemy();

												for (int i = 1; i < enemies.Length; i++)
												{
																int randomInt = Random.Range(0, 3);
																enemies[i].GetComponent<EnemyController>().enemyObject = enemyTemplates[randomInt];
																enemies[i].GetComponent<EnemyController>().ResetEnemy();
																enemies[i].GetComponent<EnemyDisplay>().enemy = enemyTemplates[randomInt];
																enemies[i].GetComponent<EnemyDisplay>().ResetEnemy();
												}
								}else if(areaNumber > areaLabels.Length-1){
												areaText.text = "Victory!";
												mainMenuButton.SetActive(true);
								}
								else
								{
												areaText.text = areaLabels[areaNumber];
												for (int i = 0; i < enemies.Length; i++)
												{
																int randomInt = Random.Range(0, 3);
																enemies[i].GetComponent<EnemyController>().enemyObject = enemyTemplates[randomInt];
																enemies[i].GetComponent<EnemyController>().ResetEnemy();
																enemies[i].GetComponent<EnemyDisplay>().enemy = enemyTemplates[randomInt];
																enemies[i].GetComponent<EnemyDisplay>().ResetEnemy();
												}
								}
								nextAreaButton.SetActive(false);
				}

				public void ReturnToMainMenu()
				{
								SceneManager.LoadScene(0);
				}
}
