﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

[System.Serializable]
public class Skill
{
				[SerializeField]
				public int value;
				[SerializeField]
				public bool attack;
				[SerializeField]
				public string name;

				public Skill(int damage, bool attack)
				{
								this.value = damage;
								this.attack = attack;
				}

				public Skill(int damage, bool attack, string name)
				{
								this.value = damage;
								this.attack = attack;
								this.name = name;
				}

				public override string ToString()
				{
								StringBuilder sb = new StringBuilder();

								if (name != null)
								{
												sb.Append(name + " | ");
								}

								sb.Append("Power: " + value + " | ");

								if (attack)
								{
												sb.Append("Attack");
								}
								else
								{
												sb.Append("Healing");
								}

								return sb.ToString();
				}
}
