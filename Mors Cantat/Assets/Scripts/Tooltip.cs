﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Tooltip : MonoBehaviour
{
				public TextMeshProUGUI toolTipBox;

				private void FixedUpdate()
				{
								Vector3 temp = Input.mousePosition;
								temp = Camera.main.ScreenToWorldPoint(temp);
								temp.z = 0f;
								temp.x += 0.5f;

								toolTipBox.transform.position = temp;
				}
}