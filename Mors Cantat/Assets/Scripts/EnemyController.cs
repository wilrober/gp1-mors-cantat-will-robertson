﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    GameObject player;

				public Enemy enemyObject;

    public int currentHealth;
				public int maxHealth;
				public int enemyID;
				public int attackDelay;

    public PlayerController playerController;

				public GameManager gameManager;

				public bool dead = false;
				public bool recentlyAttacked = true;

				SpriteRenderer spriteRender;

				public Skill skill;

    // Start is called before the first frame update
    void Start()
				{
								gameManager = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameManager>();

								enemyObject = gameManager.enemyTemplates[enemyID];

								maxHealth = enemyObject.health;
								currentHealth = maxHealth;
								skill = enemyObject.attack;
								attackDelay = enemyObject.attackDelay;

        player = GameObject.FindGameObjectWithTag("Player");
        playerController = player.GetComponent<PlayerController>();
								spriteRender = gameObject.GetComponent<SpriteRenderer>();
								spriteRender.sprite = enemyObject.enemyArt;

								Debug.Log(attackDelay);
    }

    // Update is called once per frame
    void Update()
    {
       if(currentHealth <= 0)
								{
												dead = true;
												currentHealth = 0;
												spriteRender.color = Color.black;
								}
							StartCoroutine("Attack"); 
    }

				public void OnMouseDown()
				{
								if (playerController.currentSkill.attack)
								{
												currentHealth -= playerController.currentSkill.value;
								}
								else
								{
												Debug.Log("This is not a valid target for that skill.");
								}
				}

				IEnumerator Attack()
				{
								if (skill.attack && !recentlyAttacked && !dead)
								{
												player.GetComponent<PlayerController>().currentHealth -= skill.value;
												Debug.Log(name + " attacks!");
												recentlyAttacked = true;
												yield return new WaitForSeconds((float)attackDelay);
												recentlyAttacked = false;
								}
				}

				public void ResetEnemy()
				{
								maxHealth = enemyObject.health;
								currentHealth = maxHealth;
								spriteRender.sprite = enemyObject.enemyArt;
								spriteRender.color = Color.red;
								skill = enemyObject.attack;
								dead = false;
				}
}
